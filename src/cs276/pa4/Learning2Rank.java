package cs276.pa4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import weka.classifiers.Classifier;
import weka.core.Instances;

/**
 * Main entry-point of PA4
 * Version 2.0: includes idfs_file as a command line argument
 */
public class Learning2Rank {
  static double C = Math.pow(2, -4);
  static double gamma = Math.pow(2, -2);
  /**
   * Returns a trained model
   * @param train_signal_file
   * @param train_rel_file
   * @param task
   *      1: Linear Regression
   *      2: SVM
   *      3: More features
   *      4: Extra credit
   * @param idfs
   * @return
   */
  public static Classifier train(String train_signal_file, String train_rel_file, int task, Map<String,Double> idfs) {
    System.err.println("## Training with feature_file =" + train_signal_file + ", rel_file = " + train_rel_file + " ... \n");
    Classifier model = null;
    Learner learner = null;


    if (task == 1) {
      learner = new PointwiseLearner();
    } else if (task == 2) {
      boolean isLinearKernel = true;
      //learner = new PairwiseLearner(isLinearKernel);
      learner = new PairwiseLearner(C, gamma, false, false);
    } else if (task == 3) {
      learner = new ExtendedPointwiseLearner();
      
      /* Uncomment the following line if extending PairwiseLearner */
      /*learner = new PairwiseLearner(C, gamma, false, true);*/
    } else if (task == 4) {

      /*
       * @TODO: Your code here, extra credit
       * */
      learner = new Word2Vec("word2Vec.txt");
      // System.err.println("Extra credit");

    }

    /* Step (1): construct your feature matrix here */
    Instances data = learner.extractTrainFeatures(train_signal_file, train_rel_file, idfs);

    /* Step (2): implement your learning algorithm here */
    model = learner.training(data);

    return model;
  }

  /**
   * Test model using the test signal file
   * @param test_signal_file
   * @param model
   * @param task
   * @param idfs
   * @return
   */
  public static Map<Query, List<Document>> test(String test_signal_file, Classifier model, int task, Map<String,Double> idfs){
    System.err.println("## Testing with feature_file=" + test_signal_file + " ... \n");
    Map<Query, List<Document>> ranked_queries = new HashMap<Query, List<Document>>();
    Learner learner = null;
    if (task == 1) {
      learner = new PointwiseLearner();
    } else if (task == 2) {
      boolean isLinearKernel = true;
      //learner = new PairwiseLearner(isLinearKernel);
      learner = new PairwiseLearner(C, gamma, false, false);
    } else if (task == 3) {
      learner = new ExtendedPointwiseLearner();
      
      /* Uncomment following line and comment above line to extend PairwiseLearner for task3 */
      /*learner = new PairwiseLearner(C, gamma, false, true);*/
    } else if (task == 4) {

      /*
       * @TODO: Your code here, extra credit
       * */
      learner = new Word2Vec("word2Vec.txt");
      // System.err.println("Extra credit");

    }
    /* Step (1): construct your test feature matrix here */
    TestFeatures tf = learner.extractTestFeatures(test_signal_file, idfs);

    /* Step (2): implement your prediction and ranking code here */
    ranked_queries = learner.testing(tf, model);

    return ranked_queries;
  }


  /**
   * Output the ranking results in expected format
   * @param ranked_queries
   * @param ps
   */
  public static void writeRankedResultsToFile(Map<Query,List<Document>> queryRankings,PrintStream ps) {
    for (Query query : queryRankings.keySet()) {
      StringBuilder queryBuilder = new StringBuilder();
      for (String s : query.queryWords) {
        queryBuilder.append(s);
        queryBuilder.append(" ");
      }

      String queryStr = "query: " + queryBuilder.toString() + "\n";
      ps.print(queryStr);

      for (Document res : queryRankings.get(query)) {
        res.debugStr += "headers: ";
        if (res.headers != null) {
          for (String header : res.headers){
            res.debugStr += header + ",";
          }
        }

        res.debugStr += "body: ";
        if (res.body_hits != null) {
          for ( Map.Entry<String, List<Integer>> hit : res.body_hits.entrySet()) {
            res.debugStr += hit.getKey() + "\t" + hit.getValue().size() + ",";
          }
        }

        res.debugStr += "anchor: ";
        if (res.anchors != null) {
          for (Map.Entry<String, Integer> anchor : res.anchors.entrySet()) {
            res.debugStr += anchor.getKey() + "\t" + anchor.getValue() + ",";
          }
        }
        res.debugStr +=  "body length: " + res.body_length + ",";
        res.debugStr +=  "page rank: " + res.page_rank + ",";

        String urlString =
          "  url: " + res.url + "\n" +
          "    title: " + res.title + "\n" +
          "    debug: " + res.debugStr + "\n";
        ps.print(urlString);
      }
    }
  }

  public static void main(String[] args) throws IOException {
    if (args.length != 5 && args.length != 6) {
      System.err.println("Input arguments: " + Arrays.toString(args));
      System.err.println("Usage: <train_signal_file> <train_rel_file> <test_signal_file> <idfs_file> <task> [ranked_out_file]");
      System.err.println("  ranked_out_file (optional): output results are written into the specified file. If not, output to stdout.");
      return;
    }
    String train_signal_file = args[0];
    String train_rel_file = args[1];
    String test_signal_file = args[2];
    String dfFile = args[3];
    int task = Integer.parseInt(args[4]);
    String ranked_out_file = "";
    if (args.length == 6){
      ranked_out_file = args[5];
    }

    /* Populate idfs */
    Map<String,Double> idfs = Util.loadDFs(dfFile);

    /* Train & test */
    System.err.println("### Running task" + task + "...");
    Classifier model = train(train_signal_file, train_rel_file, task, idfs);
    /* performance on the training data */
    Map<Query, List<Document>> trained_ranked_queries = test(train_signal_file, model, task, idfs);
    String trainOutFile="tmp.train.ranked";
    writeRankedResultsToFile(trained_ranked_queries, new PrintStream(new FileOutputStream(trainOutFile)));
    NdcgMain ndcg = new NdcgMain(train_rel_file);
    System.err.println("# Trained NDCG=" + ndcg.score(trainOutFile));
    (new File(trainOutFile)).delete();

    Map<Query, List<Document>> ranked_queries = test(test_signal_file, model, task, idfs);

    /* Output results */
    if(ranked_out_file == null || ranked_out_file.isEmpty()){ /* output to stdout */
      writeRankedResultsToFile(ranked_queries, System.out);
    } else {
      /* output to file */
      try {
        writeRankedResultsToFile(ranked_queries, new PrintStream(new FileOutputStream(ranked_out_file)));
      } catch (FileNotFoundException e) {
        e.printStackTrace();
      }
    }

    /*
    ndcg = new NdcgMain("/Users/xfeng/cs276/pa4-skeleton/pa4-data/pa3.rel.dev");
    Double score = ndcg.score(ranked_out_file);
    System.err.println("dev score: " + score);
    */

  }
}

