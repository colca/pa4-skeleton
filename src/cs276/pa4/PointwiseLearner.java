package cs276.pa4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import weka.classifiers.Classifier;
import weka.classifiers.functions.LinearRegression;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

/**
 * Implements point-wise learner that can be used to implement logistic regression
 *
 */
public class PointwiseLearner extends Learner {

  ArrayList<Attribute> attributes;

  public PointwiseLearner() {
      attributes = new ArrayList<Attribute>();
      attributes.add(new Attribute("url_w"));
      attributes.add(new Attribute("title_w"));
      attributes.add(new Attribute("body_w"));
      attributes.add(new Attribute("header_w"));
      attributes.add(new Attribute("anchor_w"));
      attributes.add(new Attribute("relevance_score"));
  }

  @Override
  public Instances extractTrainFeatures(String train_data_file,
      String train_rel_file, Map<String, Double> idfs) {

    FiveTuple<Instances, List<Pair<Query, Document>>, ArrayList<Attribute>, Map<Integer, List<Integer>>, Map<Query, Map<Document,Integer>>> signalData =
        Util.loadDataPoints(train_data_file, train_rel_file, idfs, attributes, true, false, null);
    Instances dataset = signalData.getFirst();
    return dataset;
  }

  @Override
  public Classifier training(Instances dataset) {
    LinearRegression model = new LinearRegression();
    try {
        model.buildClassifier(dataset);
        return model;
    } catch (Exception e) {
        System.err.println(e.getMessage());
        e.printStackTrace();
        return null;
    }
  }

  @Override
  public TestFeatures extractTestFeatures(String test_data_file,
      Map<String, Double> idfs) {
    /*
     * Create a TestFeatures object
     * Build attributes list, instantiate an Instances object with the attributes
     * Add data and populate the TestFeatures with the dataset and features
     */
    FiveTuple<Instances, List<Pair<Query, Document>>, ArrayList<Attribute>, Map<Integer, List<Integer>>, Map<Query, Map<Document,Integer>>> testSignalData =
        Util.loadDataPoints(test_data_file, null, idfs, attributes, false, false, null);

    TestFeatures testFeats = new TestFeatures();
    testFeats.features = testSignalData.getFirst();
    testFeats.index_map = testSignalData.getFifth();

    return testFeats;
  }

  @Override
  public Map<Query, List<Document>> testing(TestFeatures tf,
      Classifier model) {

    computeScores(tf, model);
    Map<Query, List<Document>> ranking = getRanking(tf);
    return ranking;
  }

  /* Accepts a labeled dataset. Returns ranked documents for each query */
  private Map<Query, List<Document>> getRanking(final TestFeatures tf) {
      /*
       * ranking
       * for each query in tf's indexMap's key set
       *    add query to ranking
       *    for each document in tf's indexMap.get(query)'s key set
       *        add document to list
       *    sort the list of documents, using the document's score as a comparison value
       * return ranking
       */
      Map<Query, List<Document>> ranking = new HashMap<Query, List<Document>>();
      for (final Query q : tf.index_map.keySet()) {
          ranking.put(q, new ArrayList<Document>());
          for (Document d : tf.index_map.get(q).keySet()) {
              ranking.get(q).add(d);
          }

          /* Sort documents according to their score */
          Collections.sort(ranking.get(q), new Comparator<Document>() {
              @Override
              public int compare(Document d1, Document d2) {
                  int d1Idx = tf.index_map.get(q).get(d1).intValue();
                  int d2Idx = tf.index_map.get(q).get(d2).intValue();
                  double difference = tf.features.instance(d2Idx).classValue() - tf.features.instance(d1Idx).classValue();
                  if (difference < 0) {
                      return -1;
                  } else if (difference > 0) {
                      return 1;
                  } else {
                      return 0;
                  }
              }
          });
      }
      return ranking;
  }

  /* Assigns labels to an unlabeled dataset */
  private void computeScores(TestFeatures tf, Classifier model) {
      for (int i = 0; i < tf.features.numInstances(); i++) {
          double score = 0.0;
          try {
              score = model.classifyInstance(tf.features.get(i));
          } catch (Exception e) {
              System.err.println(e.getMessage());
              e.printStackTrace();
          }
          tf.features.instance(i).setClassValue(score);
      }
  }

}
