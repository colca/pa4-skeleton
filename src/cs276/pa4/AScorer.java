package cs276.pa4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * An abstract class for a scorer. 
 * Needs to be extended by each specific implementation of scorers.
 */
public abstract class AScorer {

  // Map: term -> idf
  Map<String,Double> idfs; 

  // Various types of term frequencies that you will need
  String[] TFTYPES = {"url","title","body","header","anchor"};
  final String URL_STR = "url";
  final String TITLE_STR = "title";
  final String BODY_STR = "body";
  final String HEADER_STR = "header";
  final String ANCHOR_STR = "anchor";
   
  
  /**
   * Construct an abstract scorer with a map of idfs.
   * @param idfs the map of idf scores
   */
  public AScorer(Map<String,Double> idfs) {
    this.idfs = idfs;
  }

  /**
  * You can implement your own function to whatever you want for debug string
  * The following is just an example to include page information in the debug string
  * The string will be forced to be 1-line and truncated to only include the first 200 characters
  */
  public String getDebugStr(Document d, Query q)
  {
    return "Pagerank: " + Integer.toString(d.page_rank);
  }
  
    /**
     * Score each document for each query.
     * @param d the Document
     * @param q the Query
     */
  public abstract double getSimScore(Document d, Query q);
  
  /**
   * Get frequencies for a query.
   * @param q the query to compute frequencies for
   */
  public Map<String,Double> getQueryFreqs(Query q) {

    // queryWord -> term frequency
    Map<String,Double> tfQuery = new HashMap<String, Double>();     

    /*
     * TODO : Your code here
     * Compute the raw term (and/or sublinearly scaled) frequencies
     * Additionally weight each of the terms using the idf value
     * of the term in the query (we use the PA1 corpus to determine
     * how many documents contain the query terms which is stored
     * in this.idfs).
     */
    Map<String, Integer> queryWordCounts = getFrequencyMap(q.queryWords);
    for (String queryWord : q.queryWords) {
        double idf = this.idfs.containsKey(queryWord) ? this.idfs.get(queryWord) : Math.log(98998.0); /* Second case is for query terms that do not appear in corpus. */
        tfQuery.put(queryWord, queryWordCounts.get(queryWord) * idf);
    }
    
    return tfQuery;
  }
  
  
  /*
   * TODO : Your code here
   * Include any initialization and/or parsing methods
   * that you may want to perform on the Document fields
   * prior to accumulating counts.
   * See the Document class in Document.java to see how
   * the various fields are represented.
   */

  
  /**
   * Accumulate the various kinds of term frequencies 
   * for the fields (url, title, body, header, and anchor).
   * You can override this if you'd like, but it's likely 
   * that your concrete classes will share this implementation.
   * @param d the Document
   * @param q the Query
   */
  public Map<String,Map<String, Double>> getDocTermFreqs(Document d, Query q) {

    // tfs: field string -> field vector
    /* Init empty field-level vectors */
    Map<String,Map<String, Double>> tfs = new HashMap<String,Map<String, Double>>();
    for (String tfType : TFTYPES) {
        Map<String, Double> emptyVector = new HashMap<String, Double>();
        for (String queryWord : q.queryWords) {
            emptyVector.put(queryWord, 0.0);
        }
        tfs.put(tfType, emptyVector);
    }
    
    //title, url, headers term -> counts maps, respectively
    Map<String, Integer> titleWordCounts = getFrequencyMap(d.title.split("\\s+"));
    Map<String, Integer> urlTokenCounts = getFrequencyMap(d.url.split("\\W+"));
    Map<String, Integer> headerTokenCounts = d.headers == null ? null : getFrequencyMap(d.headers);
    
    
    /*
     * Your code here
     * Loop through query terms and accumulate term frequencies. 
     * Note: you should do this for each type of term frequencies,
     * i.e. for each of the different fields.
     * Don't forget to lowercase the query word.
     */
    for (String queryWord : q.queryWords) {
        queryWord = queryWord.toLowerCase();
        
        //body
        if (d.body_hits != null && d.body_hits.containsKey(queryWord)) {
            accumulateFrequency(tfs, BODY_STR, queryWord, d.body_hits.get(queryWord).size());
        }
        
        //anchors
        if (d.anchors != null) {
            for (String anchorText : d.anchors.keySet()) {
                for (String anchorWord : anchorText.split("\\s+")) {
                    anchorWord = anchorWord.toLowerCase();
                    if (queryWord.equals(anchorWord)) {
                        int anchorFreq = d.anchors.get(anchorText);
                        accumulateFrequency(tfs, ANCHOR_STR, queryWord, anchorFreq);
                    }
                }
            }
        }
        
        //title
        if (titleWordCounts.containsKey(queryWord)) {
            accumulateFrequency(tfs, TITLE_STR, queryWord, titleWordCounts.get(queryWord));
        }
        
        //url
        if (urlTokenCounts.containsKey(queryWord)) {
            accumulateFrequency(tfs, URL_STR, queryWord, urlTokenCounts.get(queryWord));
        }
        
        //headers
        if (d.headers != null && headerTokenCounts.containsKey(queryWord)) {
            accumulateFrequency(tfs, HEADER_STR, queryWord, headerTokenCounts.get(queryWord));
        }
    }
    
    return tfs;
  }
  
  double sublinearlyScale(int freq) {
      return freq == 0 ? 0.0 : 1 + Math.log(freq);
  }
  
  Map<String, Integer> getFrequencyMap(List<String> l) {
      Map<String, Integer> freqMap = new HashMap<String, Integer>();
      for (String s : l) {
          for (String token : s.split("\\s+")) {
              if (freqMap.containsKey(token)) {
                  freqMap.put(token, freqMap.get(token) + 1);
              } else {
                  freqMap.put(token, 1);
              }
          }
      }
      return freqMap;
  }
  
  Map<String, Integer> getFrequencyMap(String[] tokens) {
      Map<String, Integer> frequencyMap = new HashMap<String, Integer>();
      for (String token : tokens) {
          if (frequencyMap.containsKey(token)) {
              frequencyMap.put(token, frequencyMap.get(token) + 1);
          } else {
              frequencyMap.put(token, 1);
          }
      }
      return frequencyMap;
  }
  
  private void accumulateFrequency(Map<String, Map<String, Double>> tfs, String field, String word, int freq) {
      Map<String, Double> v = tfs.get(field);
      v.put(word, v.get(word) + freq);
  }

}
