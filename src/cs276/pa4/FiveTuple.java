package cs276.pa4;

public class FiveTuple<F, S, T, FO, FIF> {
    private F first;
    private S second;
    private T third;
    private FO fourth;
    private FIF fifth;
    
    public FiveTuple(F f, S s, T t, FO fo, FIF fif) {
        first = f;
        second = s;
        third = t;
        fourth = fo;
        fifth = fif;
    }
    
    public F getFirst() {
        return first;
    }
    
    public S getSecond() {
        return second;
    }
    
    public T getThird() {
        return third;
    }

    public FO getFourth() {
        return fourth;
    }
    
    public FIF getFifth() {
        return fifth;
    }
}
