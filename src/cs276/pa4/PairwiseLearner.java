package cs276.pa4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import weka.core.Attribute;
import weka.classifiers.Classifier;
import weka.classifiers.functions.LibSVM;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SelectedTag;
import weka.core.pmml.MappingInfo;

/**
 * Implements Pairwise learner that can be used to train SVM
 *
 */
public class PairwiseLearner extends Learner {
  private LibSVM model;
  private boolean extended = false;
  
  public PairwiseLearner(boolean isLinearKernel){
    try{
      model = new LibSVM();
    } catch (Exception e){
      e.printStackTrace();
    }

    if(isLinearKernel){
      model.setKernelType(new SelectedTag(LibSVM.KERNELTYPE_LINEAR, LibSVM.TAGS_KERNELTYPE));
    }
  }

  public PairwiseLearner(double C, double gamma, boolean isLinearKernel, boolean extended){
    this.extended = extended;
      
      try{
      model = new LibSVM();
    } catch (Exception e){
      e.printStackTrace();
    }

    model.setCost(C);
    model.setGamma(gamma); // only matter for RBF kernel
    if(isLinearKernel){
      model.setKernelType(new SelectedTag(LibSVM.KERNELTYPE_LINEAR, LibSVM.TAGS_KERNELTYPE));
    }
  }

  @Override
  public Instances extractTrainFeatures(String train_data_file,
    String train_rel_file, Map<String, Double> idfs) {
    /*
     * @TODO: Your code here:
     * Get signal file
     * Construct output dataset of type Instances
     * Add new attribute  to store relevance in the train dataset
     * Populate data
     */

    Quad<Instances, List<Pair<Query, Document>>, ArrayList<Attribute>, Map<Integer, List<Integer>>> quad =
        Util.loadSignalFile(train_data_file, idfs, extended);
    Instances X = quad.getFirst();
    List<Pair<Query, Document>> queryDocs = quad.getSecond();
    Map<Integer, List<Integer>> indexMap = quad.getFourth();

    ArrayList<Attribute> svmAttributes = new ArrayList<Attribute>();
    svmAttributes.add(new Attribute("url_w"));
    svmAttributes.add(new Attribute("title_w"));
    svmAttributes.add(new Attribute("body_w"));
    svmAttributes.add(new Attribute("header_w"));
    svmAttributes.add(new Attribute("anchor_w"));
    if (extended) {
        svmAttributes.add(new Attribute("pagerank"));
        svmAttributes.add(new Attribute("smallest_window"));
        svmAttributes.add(new Attribute("bm25f"));
    }
    ArrayList<String> labels = new ArrayList<String>();
    labels.add("+1");
    labels.add("-1");
    Attribute relevance = new Attribute("relevance", labels);
    svmAttributes.add(relevance);
    Instances SVMX = new Instances("svm_train_dataset", svmAttributes, 0);
    // set last attr as target
    SVMX.setClassIndex(SVMX.numAttributes()-1);

    int numAttributes = SVMX.numAttributes();
    int maxPositivesInClass;

    Map<String, Map<String, Double>> relMap = null;
    try {
      relMap = Util.loadRelData(train_rel_file);
    } catch (IOException e) {
      // TODO Auto-generated catch block
    }

    // for each query
    // for each doc, build up pair
    for(Map.Entry<Integer, List<Integer>> queryDocsIndex : indexMap.entrySet()) {
      int queryIndex = queryDocsIndex.getKey();

      List<Integer> docIndexes = queryDocsIndex.getValue();
      int classCounter = 0;
      maxPositivesInClass = docIndexes.size() * (docIndexes.size()-1) / (2 * 2);

      boolean bFlip = false;
      for (int i=0; i<docIndexes.size(); i++) {
        for (int j=i; j<docIndexes.size(); j++) {
          // doc index across all
          int indexX = docIndexes.get(i);
          int indexJ = docIndexes.get(j);

          String queryStr = queryDocs.get(indexX).getFirst().toString();
          String docI = queryDocs.get(indexX).getSecond().url;
          String docJ = queryDocs.get(indexJ).getSecond().url;

          Instance xI = X.get(indexX);
          Instance xJ = X.get(indexJ);

          double relI = relMap.get(queryStr).get(docI);
          double relJ = relMap.get(queryStr).get(docJ);

          double[] instance = new double[numAttributes];

          // skip equal relevance pairs
          if (relI == relJ) {
            classCounter++;
            // bFlip = false;
            continue;
          }

          // lable +1
          if (!bFlip) {
            if (relI > relJ) {
              // use i-j
              instance = diffFeatureVector(xI, xJ, numAttributes);
            } else if (relI < relJ) {
              // use j-i
              instance = diffFeatureVector(xJ, xI, numAttributes);
            }
            //attributeValues[numAttributes-1] = data.attribute(numAttributes-1).indexOfValue("+1");
            int p = SVMX.attribute(numAttributes-1).indexOfValue("+1");
            instance[numAttributes-1] = SVMX.attribute(numAttributes-1).indexOfValue("+1");
            bFlip = true;   // next one use -1 label
          }
          // lable -1
          else {
            if (relI > relJ) {
              // use j-i
              instance = diffFeatureVector(xJ, xI, numAttributes);
            } else if (relI < relJ) {
              // use i-j
              instance = diffFeatureVector(xI, xJ, numAttributes);
            }
            instance[numAttributes-1] = SVMX.attribute(numAttributes-1).indexOfValue("-1");
            bFlip = false;   // next one use +1 label
          }

          Instance inst = new DenseInstance(1.0, instance);
          SVMX.add(inst);
          classCounter++;
        }
      }
    }

    return SVMX;
  }

  private double[] diffFeatureVector(Instance i, Instance j, int numAttributes) {
    double[] instance = new double[numAttributes];
    for (int m = 0; m < numAttributes-1; ++m){
      double featureI = i.value(m);
      double featureJ = j.value(m);
      instance[m] = featureI - featureJ;
    }

    return instance;
  }

  @Override
  public Classifier training(Instances dataset) {
    /*
     * @TODO: Your code here
     * Build classifer
     */
    System.out.println("Start building model");
    try {
      model.buildClassifier(dataset);
    } catch (Exception e) {
      // TODO Auto-generated catch block
    }
    System.out.println("Finish building model");

    return model;
  }

  @Override
  public TestFeatures extractTestFeatures(String test_data_file,
      Map<String, Double> idfs) {
    /*
     * @TODO: Your code here
     * Use this to build the test features that will be used for testing
     */
    Quad<Instances, List<Pair<Query, Document>>, ArrayList<Attribute>, Map<Query, Map<Document, Integer>>> quad =
        Util.loadTestSignalFile(test_data_file, idfs, this.extended);
    TestFeatures testSet = new TestFeatures();
    testSet.index_map = quad.getFourth();
    testSet.features = quad.getFirst();
    return testSet;
  }

  private int comparator(TestFeatures tf, Classifier model, Query query, Document doc1, Document doc2) {
    Instance x1 = tf.features.get(tf.index_map.get(query).get(doc1));
    Instance x2 = tf.features.get(tf.index_map.get(query).get(doc2));
    double[] diff = diffFeatureVector(x1, x2, x1.numAttributes());

    Instance inst = new DenseInstance(1.0, diff);
    inst.setDataset(tf.features);
    double classification = 1.0;
    try {
      classification = model.classifyInstance(inst);
    } catch (Exception e) {
      // TODO Auto-generated catch block
    }

    return (classification > 0.0) ? 1 : -1;

  }

  @Override
  public Map<Query, List<Document>> testing(final TestFeatures tf,
      final Classifier model) {
    /*
     * @TODO: Your code here
     */
    Map<Query, List<Document>> rankedResults = new HashMap<Query, List<Document>>();
    for(Map.Entry<Query, Map<Document, Integer>> entry : tf.index_map.entrySet()) {
      final Query q = entry.getKey();
      List<Document> docs = new ArrayList<Document>();
      for (Document doc : entry.getValue().keySet()) {
        docs.add(doc);
      }
      Collections.sort(docs, new Comparator<Document>() {
        @Override
        public int compare(Document doc1, Document doc2) {
          return comparator(tf, model, q, doc1, doc2);
        }
      });
      rankedResults.put(q, docs);
    }

    return rankedResults;
  }

}
