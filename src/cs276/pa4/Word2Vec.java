package cs276.pa4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import weka.core.Attribute;
import weka.core.Instances;

public class Word2Vec extends ExtendedPointwiseLearner{
  Embedding _embedding;

  public Word2Vec(String word2VecFile) {
    super();
    // remove relevance_score feature
    this.attributes.remove(this.attributes.size()-1);

    attributes.add(new Attribute("title_cos_sim"));
    attributes.add(new Attribute("header_cos_sim"));
    attributes.add(new Attribute("body_cos_sim"));
    attributes.add(new Attribute("anchor_cos_sim"));

    attributes.add(new Attribute("relevance_score"));

    loadEmbeddings(word2VecFile);
  }

  @Override
  public Instances extractTrainFeatures(String train_data_file,
      String train_rel_file, Map<String, Double> idfs) {

    FiveTuple<Instances, List<Pair<Query, Document>>, ArrayList<Attribute>, Map<Integer, List<Integer>>, Map<Query, Map<Document,Integer>>> signalData =
        Util.loadDataPoints(train_data_file, train_rel_file, idfs, attributes, true, true, _embedding);
    Instances dataset = signalData.getFirst();
    return dataset;
  }

  @Override
  public TestFeatures extractTestFeatures(String test_data_file,
          Map<String, Double> idfs) {
      /*
       * Create a TestFeatures object
       * Build attributes list, instantiate an Instances object with the attributes
       * Add data and populate the TestFeatures with the dataset and features
       */
      FiveTuple<Instances, List<Pair<Query, Document>>, ArrayList<Attribute>, Map<Integer, List<Integer>>, Map<Query, Map<Document,Integer>>> testSignalData =
          Util.loadDataPoints(test_data_file, null, idfs, attributes, false, true, _embedding);

      TestFeatures testFeats = new TestFeatures();
      testFeats.features = testSignalData.getFirst();
      testFeats.index_map = testSignalData.getFifth();

      return testFeats;
  }

  private void loadEmbeddings(String word2VecFile) {
    // String wordVectorFile = "/Users/xfeng/cs276/pa4-skeleton/word2Vec.txt";
    _embedding = new Embedding(word2VecFile, false);
  }
}
