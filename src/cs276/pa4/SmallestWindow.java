package cs276.pa4;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;


/*  
 * Class for finding smallest window.
 */
public class SmallestWindow {
    
    static private boolean fewerListsThanQueryTerms(Document d, Query q) {
        if (d.body_hits == null) {
            return true;
        }
        return d.body_hits.size() < q.queryWords.size();
    }
    
    static private void decrementMapCount(Map<String, Integer> m, String word) {
        if (!m.containsKey(word)) {
            m.put(word, 0);
        }
        m.put(word, m.get(word) - 1);
    }
    
    static private boolean duplicateFirstWord(Deque<Pair<String, Integer>> block, Map<String, Integer> queryTermCountsInBlock) {
        String frontWord = block.peekFirst().getFirst();
        return queryTermCountsInBlock.get(frontWord) > 1;
    }
    
    static private boolean containsAllQueryTerms(Deque<Pair<String, Integer>> block, Query q) {
        //Note: Slow next five lines
        Iterator<Pair<String, Integer>> i = block.iterator();
        Set<String> blockWords = new HashSet<String>();
        while (i.hasNext()) {
            blockWords.add(i.next().getFirst());
        }
        
        boolean containsAll = true;
        for (String queryWord : q.queryWords) {
            if (!blockWords.contains(queryWord)) {
                containsAll = false;
                break;
            }
        }
        return containsAll;
    }
    
    static private void normalizeBlock(Deque<Pair<String, Integer>> block, Map<String, Integer> queryTermCountsInBlock) {
        if (block.size() < 2) {
            return;
        }
        while (duplicateFirstWord(block, queryTermCountsInBlock)) {
            Pair<String, Integer> front = block.pollFirst();
            decrementMapCount(queryTermCountsInBlock, front.getFirst());
        }
    }
    
    static private void incrementMapCount(Map<String, Integer> m, String word) {
        if (!m.containsKey(word)) {
            m.put(word, 0);
        }
        m.put(word, m.get(word) + 1);
    }
    
    static private Map<String, Pair<Integer, List<Integer>>> getIndexPositionsPairs(Document d) {
        Map<String, Pair<Integer, List<Integer>>> indexPositionsPairs = new HashMap<String, Pair<Integer, List<Integer>>>();
        for (Entry<String, List<Integer>> e : d.body_hits.entrySet()) {
            indexPositionsPairs.put(e.getKey(), new Pair<Integer, List<Integer>>(0, e.getValue()));
        }
        return indexPositionsPairs; 
    }
    
    static private Map<String, Integer> initQueryTermCountInBlock(Query q) {
        Map<String, Integer> queryTermCountInBlock = new HashMap<String, Integer>();
        for (String queryWord : q.queryWords) {
            queryTermCountInBlock.put(queryWord, 0);
        }
        return queryTermCountInBlock;
    }
    
    static private int getNumPositions(Document d) {
        int numPositions = 0;
        for (Entry<String, List<Integer>> e : d.body_hits.entrySet()) {
            numPositions += e.getValue().size();
        }
        return numPositions;
    }
    
    static private Pair<String, Integer> getNextWordAndPosition(Map<String, Pair<Integer, List<Integer>>> indexPositionsPairs) {
        Pair<String, Integer> nextWordAndPosition = new Pair("", Integer.MAX_VALUE);
        for (Entry<String, Pair<Integer, List<Integer>>> e : indexPositionsPairs.entrySet()) {
            String word = e.getKey();
            int index = e.getValue().getFirst();
            List<Integer> positions = e.getValue().getSecond();
            
            if (index < positions.size() && positions.get(index) < nextWordAndPosition.getSecond()) {
                nextWordAndPosition.setFirst(word);
                nextWordAndPosition.setSecond(positions.get(index));
            }
        }
        
        //basic check
        if (nextWordAndPosition.getFirst().equals("")) {
            System.out.println("Error getNextWordAndPosition");
        }
        
        //Increment index of chosen element
        int prevIndex = indexPositionsPairs.get(nextWordAndPosition.getFirst()).getFirst();
        indexPositionsPairs.get(nextWordAndPosition.getFirst()).setFirst(prevIndex + 1);
        
        return nextWordAndPosition;
    }
    
    static private int calculateWindowSize(Deque<Pair<String, Integer>> block) {
        return block.peekLast().getSecond() - block.peekFirst().getSecond() + 1;
    }
    
    static private int getBodyWindow(Document d, Query q) {
        if (fewerListsThanQueryTerms(d, q)) {
            return Integer.MAX_VALUE;
        }
        
        Map<String, Pair<Integer, List<Integer>>> indexPositionsPairs = getIndexPositionsPairs(d); //maps query term -> (iteration index, positions list)
        Map<String, Integer> queryTermCountInBlock = initQueryTermCountInBlock(q);
        Deque<Pair<String, Integer>> block = new ArrayDeque<Pair<String, Integer>>();
        int numPositions = getNumPositions(d);
        int positionsProcessed = 0;
        int minimumWindowSize = Integer.MAX_VALUE;
        
        while (positionsProcessed < numPositions) {
            Pair<String, Integer> nextWordAndPosition = getNextWordAndPosition(indexPositionsPairs);
            block.add(nextWordAndPosition);
            incrementMapCount(queryTermCountInBlock, nextWordAndPosition.getFirst());
            normalizeBlock(block, queryTermCountInBlock);
            if (containsAllQueryTerms(block, q)) {
                minimumWindowSize = Math.min(minimumWindowSize, calculateWindowSize(block));
            }
            positionsProcessed++;
        }
        
        return minimumWindowSize;
    }
    
    
    
    static private int getStringArrayWindow(Query q, String[] strArr) {
        Deque<Pair<String, Integer>> block = new ArrayDeque<Pair<String, Integer>>();
        Set<String> queryWords = new HashSet<String>(q.queryWords);
        Map<String, Integer> queryTermCountInBlock = initQueryTermCountInBlock(q);
        int minimumWindow = Integer.MAX_VALUE;
        
        for (int i = 0; i < strArr.length; i++) {
            String s = strArr[i];
            
            if (queryWords.contains(s)) {
                block.offerLast(new Pair<String, Integer>(s, i));
                incrementMapCount(queryTermCountInBlock, s);
                normalizeBlock(block, queryTermCountInBlock);
                if (containsAllQueryTerms(block, q)) {
                    minimumWindow = Math.min(minimumWindow, calculateWindowSize(block));
                }
            }
        }
        
        return minimumWindow;
    }
    
    static private int getUrlWindow(Document d, Query q) {
        return getStringArrayWindow(q, d.url.split("\\W+"));
    }
    
    static private int getTitleWindow(Document d, Query q) {
        return getStringArrayWindow(q, d.title.split("\\s+"));
    }
    
    static private int getHeadersWindow(Document d, Query q) {
        int minWindow = Integer.MAX_VALUE;
        if (d.headers != null) {
            for (String header : d.headers) {
                minWindow = Math.min(minWindow, getStringArrayWindow(q, header.split("\\s+")));
            }
        }
        return minWindow;
    }
    
    static private int getAnchorsWindow(Document d, Query q) {
        int minWindow = Integer.MAX_VALUE;
        if (d.anchors != null) {
            for (String anchor : d.anchors.keySet()) {
                minWindow = Math.min(minWindow, getStringArrayWindow(q, anchor.split("\\s+")));
            }
        }
        return minWindow;
    }
    
    static int getSmallestWindow(Document d, Query q) {
        int minimumWindow = Integer.MAX_VALUE;
          
        //get minimum url, title, header, anchor, and body windows, respectively
        minimumWindow = Math.min(minimumWindow, getUrlWindow(d, q));
        minimumWindow = Math.min(minimumWindow, getTitleWindow(d, q));
        minimumWindow = Math.min(minimumWindow, getHeadersWindow(d, q));
        minimumWindow = Math.min(minimumWindow, getAnchorsWindow(d, q));
        minimumWindow = Math.min(minimumWindow, getBodyWindow(d, q));
        
        return minimumWindow;
    }

}
