package cs276.pa4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* Class to extract feature vector given a document and a query */
public class Feature {

  public static boolean isSublinearScaling = true;
  private Parser parser = new Parser();
  double smoothingBodyLength = 800;

  Map<String,Double> idfs;
  BM25FScorer bm25fScorer = null;

  // If you would like to use additional features, you can declare necessary variables here
  /*
   * @TODO: Your code here
   */

  public Feature(Map<String,Double> idfs){
    this.idfs = idfs;
  }

  public Feature(Map<String, Double> idfs, Map<Query, Map<String, Document>> queryDict) {
      this.idfs = idfs;
      this.bm25fScorer = new BM25FScorer(idfs, queryDict);
  }

  public double[] extractFeatureVector(Document d, Query q){

    /* Compute doc_vec and query_vec */
    Map<String,Map<String, Double>> tfs = Util.getDocTermFreqs(d,q);
    Map<String,Double> queryVector = getQueryVec(q);

    // normalize term-frequency
    this.normalizeTFs(tfs, d, q);

    /* [url, title, body, header, anchor] */
    double[] result = new double[5];
    for (int i = 0; i < result.length; i++) { result[i] = 0.0; }
    for (String queryWord : q.queryWords){
      double queryScore = queryVector.get(queryWord);
      result[0]  += tfs.get("url").get(queryWord) * queryScore;
      result[1]  += tfs.get("title").get(queryWord) * queryScore;
      result[2]  += tfs.get("body").get(queryWord) * queryScore;
      result[3]  += tfs.get("header").get(queryWord) * queryScore;
      result[4]  += tfs.get("anchor").get(queryWord) * queryScore;
    }

    return result;
  }

  /* Generate query vector */
  public Map<String,Double> getQueryVec(Query q) {
    /* Count word frequency within the query, in most cases should be 1 */

    Map<String, Double> tfVector = new HashMap<String, Double>();
    String[] wordInQuery = q.query.toLowerCase().split(" ");
    for (String word : wordInQuery){
      if (tfVector.containsKey(word))
        tfVector.put(word, tfVector.get(word) + 1);
      else
        tfVector.put(word, 1.0);
    }

    /* Sublinear Scaling */
    if(isSublinearScaling){
      for (String word : tfVector.keySet()) {
        tfVector.put(word, 1 + Math.log(tfVector.get(word)));
      }
    }

    /* Compute idf vector */
    Map<String,Double> idfVector = new HashMap<String,Double>();

    for (String queryWord : q.queryWords) {
      if (this.idfs.containsKey(queryWord))
        idfVector.put(queryWord, this.idfs.get(queryWord));
      else {
        idfVector.put(queryWord, Math.log(98998.0)); /* Laplace smoothing */
      }

    }

    /* Do dot-product */
    Map<String, Double> queryVector = new HashMap<String, Double>();
    for (String word : q.queryWords) {
      queryVector.put(word, tfVector.get(word) * idfVector.get(word));
    }

    return queryVector;
  }

  public void normalizeTFs(Map<String,Map<String, Double>> tfs,Document d, Query q)
  {
    double normalizationFactor = (double)(d.body_length) + (double)(smoothingBodyLength);

    for (String queryWord : q.queryWords)
      for (String tfType : tfs.keySet())
        tfs.get(tfType).put(queryWord, tfs.get(tfType).get(queryWord)/normalizationFactor);
  }


  double getPageRank(Document d) {
      return d.page_rank;
  }

  double getMinBodyHit(Document d, Query q) {

    int bodyHitsCount = Integer.MAX_VALUE;
    int totalCounts = 0;
    for (String word : q.queryWords) {
      if (d.body_hits == null) {
        continue;
      }
      List<Integer> bodyHits = d.body_hits.get(word);
      if (bodyHits == null || bodyHits.size() <= 0) {
        continue;
      }
      bodyHitsCount = Math.min(bodyHitsCount, bodyHits.size());
      totalCounts += bodyHits.size();
    }
    double ratio = 0d;
    if (totalCounts > 0) {
      ratio = bodyHitsCount / totalCounts;
    }
    return ratio;
  }

  double getBodyHitTermCount(Document d, Query q) {
    double termHitCount = 0;
    for (String word : q.queryWords) {
      if (d.body_hits != null && d.body_hits.containsKey(word)) {
        termHitCount++;
      }
    }
    return termHitCount;
  }

  public double[] extractMoreFeatures(Document d, Query q) {
      double[] basic = extractFeatureVector(d, q);

      double[] more = new double[5];
      more[0] = getPageRank(d);
      more[1] = SmallestWindow.getSmallestWindow(d, q);
      more[2] = bm25fScorer.getSimScore(d, q);
      more[3] = getMinBodyHit(d, q);
      more[4] = getBodyHitTermCount(d, q);
      
      /* If extending PointwiseLearner, uncomment following block and comment out above block */
      /*double[] more = new double[3];
      more[0] = getPageRank(d);
      more[1] = SmallestWindow.getSmallestWindow(d, q);
      more[2] = bm25fScorer.getSimScore(d, q);*/

      /* Concatenate arrays basic and more */
      double[] result = new double[basic.length + more.length];
      for (int i = 0; i < basic.length; i++) {
          result[i] = basic[i];
      }
      for (int i = basic.length; i < result.length; i++) {
          result[i] = more[i - basic.length];
      }

      return result;
  }

  private double getAvgTitleCosSimilarity(Document d, Query q, Embedding embedding) {
    double cosSim = 0d;
    int counter = 0;
    for (String word : q.queryWords) {
      String[] titleTerms = d.title.trim().split(" ");
      for (String title : titleTerms) {
        cosSim += embedding.getCosSimilarity(word, title);
        counter++;
      }
    }
    return counter != 0 ? cosSim/counter : 0;
  }

  private double getAvgHeaderCosSimilarity(Document d, Query q, Embedding embedding) {
    double cosSim = 0d;
    int counter = 0;
    for (String word : q.queryWords) {
      if (d.headers == null) {
        break;
      }
      for(String header : d.headers) {
        String[] headerTerms = header.trim().split(" ");
        for (String term : headerTerms) {
          cosSim += embedding.getCosSimilarity(word, term);
          counter++;
        }
      }
    }
    return counter != 0 ? cosSim/counter : 0;
  }

  private double getAvgBodyCosSimilarity(Document d, Query q, Embedding embedding) {
    double cosSim = 0d;
    int counter = 0;
    for (String word : q.queryWords) {
      if (d.body_hits == null) {
        break;
      }
      for(Map.Entry<String, List<Integer>> bodyHit : d.body_hits.entrySet()) {
        String body = bodyHit.getKey();
        cosSim += embedding.getCosSimilarity(word, body);
        counter++;
      }
    }
    return counter != 0 ? cosSim/counter : 0;
  }

  private double getAvgAnchorCosSimilarity(Document d, Query q, Embedding embedding) {
    double cosSim = 0d;
    int counter = 0;
    for (String word : q.queryWords) {
      if (d.anchors == null) {
        break;
      }
      for (Map.Entry<String, Integer> anchor : d.anchors.entrySet()) {
        cosSim += embedding.getCosSimilarity(word, anchor.getKey());
        counter++;
      }
    }
    return counter != 0 ? cosSim/counter : 0;
  }

  public double[] extractEmbeddingFeatures(Document d, Query q, Embedding embedding) {
    double[] basic = extractMoreFeatures(d, q);

    double[] embeddingFeatures = new double[4];
    embeddingFeatures[0] = getAvgTitleCosSimilarity(d, q, embedding);
    embeddingFeatures[1] = getAvgHeaderCosSimilarity(d, q, embedding);
    embeddingFeatures[2] = getAvgBodyCosSimilarity(d, q, embedding);
    embeddingFeatures[3] = getAvgAnchorCosSimilarity(d, q, embedding);
    // Concatenate
    double[] res = new double[basic.length + embeddingFeatures.length];
    for (int i = 0; i < basic.length; i++) {
      res[i] = basic[i];
    }
    for (int i = basic.length; i < res.length; i++) {
      res[i] = embeddingFeatures[i - basic.length];
    }
    return res;
  }

}
