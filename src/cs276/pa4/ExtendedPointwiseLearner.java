package cs276.pa4;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import weka.core.Attribute;
import weka.core.Instances;

public class ExtendedPointwiseLearner extends PointwiseLearner {

    protected ArrayList<Attribute> attributes;

    public ExtendedPointwiseLearner() {
        attributes = new ArrayList<Attribute>();
        attributes.add(new Attribute("url_w"));
        attributes.add(new Attribute("title_w"));
        attributes.add(new Attribute("body_w"));
        attributes.add(new Attribute("header_w"));
        attributes.add(new Attribute("anchor_w"));
        attributes.add(new Attribute("pagerank"));
        attributes.add(new Attribute("smallest_window"));
        attributes.add(new Attribute("bm25f_score"));
        attributes.add(new Attribute("skewed_hit"));
        attributes.add(new Attribute("bodyhit_term_count"));

        attributes.add(new Attribute("relevance_score"));
    }

    @Override
    public Instances extractTrainFeatures(String train_data_file,
        String train_rel_file, Map<String, Double> idfs) {

      FiveTuple<Instances, List<Pair<Query, Document>>, ArrayList<Attribute>, Map<Integer, List<Integer>>, Map<Query, Map<Document,Integer>>> signalData =
          Util.loadDataPoints(train_data_file, train_rel_file, idfs, attributes, true, true, null);
      Instances dataset = signalData.getFirst();
      return dataset;
    }

    @Override
    public TestFeatures extractTestFeatures(String test_data_file,
            Map<String, Double> idfs) {
        /*
         * Create a TestFeatures object
         * Build attributes list, instantiate an Instances object with the attributes
         * Add data and populate the TestFeatures with the dataset and features
         */
        FiveTuple<Instances, List<Pair<Query, Document>>, ArrayList<Attribute>, Map<Integer, List<Integer>>, Map<Query, Map<Document,Integer>>> testSignalData =
            Util.loadDataPoints(test_data_file, null, idfs, attributes, false, true, null);

        TestFeatures testFeats = new TestFeatures();
        testFeats.features = testSignalData.getFirst();
        testFeats.index_map = testSignalData.getFifth();

        return testFeats;
    }
}
