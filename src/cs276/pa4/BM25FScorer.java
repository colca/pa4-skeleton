package cs276.pa4;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Skeleton code for the implementation of a BM25 Scorer in Task 2.
 */
public class BM25FScorer extends AScorer {

  /*
   *  TODO: You will want to tune these values
   */
  /*double urlweight = 0.05;
  double titleweight  = 0.35;
  double bodyweight = 0.05;
  double headerweight = 0.1;
  double anchorweight = 0.2;
  double smoothingBodyLength = 50.0; */

  double urlweight = 0.05;
  double titleweight  = 0.35;
  double bodyweight = 0.05;
  double headerweight = 0.1;
  double anchorweight = 0.2;

  // BM25-specific weights
  double burl = 0.99;
  double btitle = 0.01;
  double bheader = 0.01;
  double bbody = 0.25;
  double banchor = 0.5;

  double k1 = 2;
  double pageRankLambda = 0.3;
  double pageRankLambdaPrime = 0.1;
  double k3 = 0.1;
  double pageRankLambdaPrimePrime = 1.5;

  // query -> url -> document
  Map<Query,Map<String, Document>> queryDict;

  // BM25 data structures--feel free to modify these
  // Document -> field -> length
  Map<Document,Map<String,Double>> lengths;

  // field name -> average length
  Map<String,Double> avgLengths;

  // field name -> total length
  Map<String,Double> totalLengths;

  // Document -> pagerank score
  Map<Document,Double> pagerankScores;

    /**
     * Construct a BM25FScorer.
     * @param idfs the map of idf scores
     * @param queryDict a map of query to url to document
     */
    public BM25FScorer(Map<String,Double> idfs, Map<Query,Map<String, Document>> queryDict) {
      super(idfs);
      this.queryDict = queryDict;
      this.calcAverageLengths();
    }


  private void populateFieldLenAndPageRank(Document doc, Map<String, Double> totalLengths, Map<Document,Map<String,Double>> lengths,
      Map<Document,Double> pagerankScores) {

    int len = 0;
    Map<String, Double> docFieldLenMap = new HashMap<String, Double>();
    for (String tfType : this.TFTYPES) {
      // String[] TFTYPES = {"url","title","body","header","anchor"};
      if (tfType.contains("url")) {
        // http://cs.stanford.edu/people/eroberts/mscsed/Admissions-MSInCSEducation.html
        String[] sections = doc.url.split("\\W+");
        len = sections.length;
        totalLengths.put("url", totalLengths.getOrDefault("url", 0d) + len);
        docFieldLenMap.put("url", (double) len);
      }
      else if (tfType.contains("title")) {
        len = doc.title.split("\\s+").length;
        totalLengths.put("title", totalLengths.getOrDefault("title", 0d) + len);
        docFieldLenMap.put("title", (double) len);
      }
      else if (tfType.contains("body")) {
        totalLengths.put("body", totalLengths.getOrDefault("body", 0d) + doc.body_length);
        docFieldLenMap.put("body", (double) doc.body_length);
      }
      else if (tfType.contains("header")) {
        if (doc.headers == null) {
          continue;
        }
        for (String header : doc.headers) {
          len = header.split("\\s+").length;
          totalLengths.put("header", totalLengths.getOrDefault("header", 0d) + len);
          docFieldLenMap.put("header", docFieldLenMap.getOrDefault("header", 0d) + len);
        }
      }
      else if (tfType.contains("anchor")) {
        if(doc.anchors == null) {
          continue;
        }
        for (Map.Entry<String, Integer> anchorEntry : doc.anchors.entrySet()) {
          len = (anchorEntry.getKey().split("\\s+").length) * anchorEntry.getValue();
          //len = anchorEntry.getKey().split("\\s+").length;
          totalLengths.put("anchor", totalLengths.getOrDefault("anchor", 0d) + len);
          docFieldLenMap.put("anchor", docFieldLenMap.getOrDefault("anchor", 0d) + len);
        }
      }
    }
    lengths.put(doc, docFieldLenMap);
    pagerankScores.put(doc, (double) doc.page_rank);
  }

    /**
     * Set up average lengths for BM25, also handling PageRank.
     */
  public void calcAverageLengths() {
    lengths = new HashMap<Document,Map<String,Double>>();
    avgLengths = new HashMap<String,Double>();
    pagerankScores = new HashMap<Document,Double>();

    /*
     * TODO : Your code here
     * Initialize any data structures needed, perform
     * any preprocessing you would like to do on the fields,
     * handle pagerank, accumulate lengths of fields in documents.
     */

    totalLengths = new HashMap<String, Double>();
    // Get document set of full corpus
    Map<String, Document> urlDocMap = new HashMap<String, Document>();
    for (Map.Entry<Query, Map<String, Document>> queryDoc : queryDict.entrySet()) {
      Map<String, Document> docs = queryDoc.getValue();
      for (Map.Entry<String, Document> docEntry : docs.entrySet()) {
        String url = docEntry.getKey();
        Document doc = docEntry.getValue();
        // Count each document once
        if (!urlDocMap.containsKey(url)) {
          urlDocMap.put(url, doc);
          populateFieldLenAndPageRank(doc, totalLengths, lengths, pagerankScores);
        }
      }
    }

    Double avgLen = 0d;
    for (String tfType : this.TFTYPES) {
    /*
     * TODO : Your code here
     * Normalize lengths to get average lengths for
     * each field (body, url, title, header, anchor)
     */
      avgLen = totalLengths.getOrDefault(tfType, 0d) / urlDocMap.entrySet().size();
      if (-0.01 <= (avgLen - 0d) && (avgLen - 0d) <= 0.01) {
        System.out.println("Error: Avg length for field " + tfType + " is " + (avgLen-0d));
      }
      avgLengths.put(tfType, avgLen);
    }
  }

  /**
   * Get the net score.
   * @param tfs the term frequencies
   * @param q the Query
   * @param tfQuery
   * @param d the Document
   * @return the net score
   */
  public double getNetScore(Map<String,Map<String, Double>> tfs, Query q, Map<String,Double> tfQuery,Document d) {

    double score = 0.0;

    /*
     * TODO : Your code here
     * Use equation 5 in the writeup to compute the overall score
     * of a document d for a query q.
     */
    double w = 0d;
    double temp = 0d;
    double queryTemp = 0d;
    for (String term : q.queryWords) {
      //term = term.toLowerCase();
      w = 0d;
      for (String tfType : this.TFTYPES) {
        w += selectWeight(tfType) * tfs.get(tfType).getOrDefault(term, 0d);
      }
      temp += (w * idfs.getOrDefault(term, 0d)) / (k1 + w);
      // Incorporate query info
      queryTemp = (k3+1) * tfQuery.get(term) / (k3 + tfQuery.get(term));
      temp = temp * queryTemp;
    }

    // log
    score = temp + pageRankLambda * Math.log(pageRankLambdaPrime + pagerankScores.getOrDefault(d, 0d));
    // f / (lamdaPrime + f)
    //double pagerank = pagerankScores.getOrDefault(d, 0d);
    // score = temp + pageRankLambda * ( pagerank / (pagerank + pageRankLambdaPrime));

    // 1 / (lamdaPrime + exp(-f * lamdaPrimePrime))
    //score = temp + pageRankLambda * (1 / (pageRankLambda + Math.exp(-pagerank * pageRankLambdaPrimePrime)));
    return score;
  }

  private double selectWeight(String tfType) {
    if (tfType.contains("url")) {
      return urlweight;
    }
    else if (tfType.contains("title")) {
      return titleweight;
    }
    else if (tfType.contains("body")) {
      return bodyweight;
    }
    else if (tfType.contains("header")) {
      return headerweight;
    }
    else if (tfType.contains("anchor")) {
      return anchorweight;
    }
    return 0d;
  }

  /**
   * Do BM25 Normalization.
   * @param tfs the term frequencies tf type -> queryWord -> score
   * @param d the Document
   * @param q the Query
   */
  public void normalizeTFs(Map<String,Map<String, Double>> tfs,Document d, Query q) {
  /*
   * TODO : Your code here
   * Use equation 3 in the writeup to normalize the raw term frequencies
   * in fields in document d.
   */
   Double ftf = 0d;  // field dependent tf
   Double tf = 0d;   // raw field tf
   Map<String, Double> docFieldLenMap = lengths.getOrDefault(d, new HashMap<String, Double>());
   for (String term : q.queryWords) {
     term = term.toLowerCase();
     for (String tfType : this.TFTYPES) {
       tf = tfs.get(tfType).get(term);

       ftf = 1 + selectB(tfType) * (docFieldLenMap.getOrDefault(tfType, 0d) / avgLengths.get(tfType) - 1);
       // ftf = 1 + selectB(tfType) * (docFieldLenMap.get(tfType) / avgLengths.get(tfType) - 1);
       ftf = tf / ftf;
       // Update tfs with normalized field dependent term frequency
       tfs.get(tfType).put(term, ftf);
     }
   }
  }

  private double selectB(String tfType) {
    if (tfType.contains("url")) {
      return burl;
    }
    else if (tfType.contains("title")) {
      return btitle;
    }
    else if (tfType.contains("body")) {
      return bbody;
    }
    else if (tfType.contains("header")) {
      return bheader;
    }
    else if (tfType.contains("anchor")) {
      return banchor;
    }
    return 0d;
  }

  /**
   * Write the tuned parameters of BM25 to file.
   * Only used for grading purpose, you should NOT modify this method.
   * @param filePath the output file path.
   */
  private void writeParaValues(String filePath) {
    try {
      File file = new File(filePath);
      if (!file.exists()) {
        file.createNewFile();
      }
      FileWriter fw = new FileWriter(file.getAbsoluteFile());
      String[] names = {
        "urlweight", "titleweight", "bodyweight",
        "headerweight", "anchorweight", "burl", "btitle",
        "bheader", "bbody", "banchor", "k1", "pageRankLambda", "pageRankLambdaPrime"
      };
      double[] values = {
        this.urlweight, this.titleweight, this.bodyweight,
        this.headerweight, this.anchorweight, this.burl, this.btitle,
        this.bheader, this.bbody, this.banchor, this.k1, this.pageRankLambda,
        this.pageRankLambdaPrime
      };
      BufferedWriter bw = new BufferedWriter(fw);
      for (int idx = 0; idx < names.length; ++ idx) {
        bw.write(names[idx] + " " + values[idx]);
        bw.newLine();
      }
      bw.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Override
  /**
   * Get the similarity score.
   * @param d the Document
   * @param q the Query
   * @return the similarity score
   */
  public double getSimScore(Document d, Query q) {
    Map<String,Map<String, Double>> tfs = this.getDocTermFreqs(d,q);
    this.normalizeTFs(tfs, d, q);
    Map<String,Double> tfQuery = getQueryFreqs(q);

    // Write out the tuned BM25 parameters
    // This is only used for grading purposes.
    // You should NOT modify the writeParaValues method.
    writeParaValues("bm25Para.txt");
    return getNetScore(tfs,q,tfQuery,d);
  }

}
